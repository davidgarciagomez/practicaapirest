/****************************************/
/* DEFINICIÓN DE DEPENCENCIAS           */
/****************************************/
var express = require('express');
var fs = require('fs');
/* Inicializamos el framework expresss para utilizarlo*/
var app = express();
/* Parseador del cuerpo de las peticiones*/
var bodyParser = require('body-parser');
app.use(bodyParser.json());
/* Por definicion establecemos que nuestra app escuche en el puerto 3000*/
var port = process.env.PORT || 3000;
/*Usuarios*/
var users = [{"codUser":"e000000"}];
//Base de la url de peticiones a MLAB.
var mLabBaseURL = "https://api.mlab.com/api/1/databases/apitechudgg/collections/";
/* Api key de mlab para el consumo de */
var mLabAPIKey = "apiKey=w_woORSGTtk09y9gDPPzXM_oVrtwEGJB";
/*Librería de tratamiento de llamadas json*/
var requestJson =  require("request-json");
/* Arrancamos el framework de express */
app.listen(port);
/* Mostramos el fin de aranque*/
console.log("API Molona escuchando en el puerto " + port);

/****************************************/
/* MANEJADORES DE RUTAS DE ACCESO       */
/****************************************/

/** Manejador de la ruta /apitechu/v1, con su funcion inline manejadora
*   de la peticion.
*/
app.get("/apitechu/v1",
  function(req,res){
    console.log("GET apitechu/v1");
    res.send({ "msg" : "hola desde apitechu/v1" });
  }
);

/********* USERS LIST */
app.get("/apitechu/v1/users",
  function (req, res){
    console.log("GET /apitechu/v1/users");
    res.sendFile("./data/usuarios.json",{root:__dirname});
    //var users = require('./data/usuarios.json');
    //res.send(users);
  }
);

/********* USERS CREATE */
app.post("/apitechu/v1/users",
  function (req, res){
    console.log("POST /apitechu/v1/users");

    //Validaciones de entrada
    console.log("req.body.last_name:["+req.body.last_name+"]");
    console.log("req.body.first_name:["+req.body.first_name+"]");
    console.log("req.body.country:["+req.body.country+"]");

    //Creacion del usuario
    var newUser = {
      "first_name" : req.body.first_name,
      "last_name" : req.body.last_name,
      "country"  : req.body.country
    };
    var users = require('./data/usuarios.json');
    users.push(newUser);
    wireteUserDataToFile(users);
    res.send({"msg" : "Usuario guardado con exito"}   );
  }
);


/********* USERS DELETE */
app.delete("/apitechu/v1/users/:id",
  function (req, res){
    console.log("DELETE /apitechu/v1/users/:id");
    console.log(req.params.id);
    var users = require('./data/usuarios.json');
    users.splice(req.params.id-1,1);
    wireteUserDataToFile(users);
    console.log("Usuario borrado");
    res.send({"msg" : "Usuario eliminado con exito"}   );
  }
);

/********* USERS LOGIN */
app.post("/apitechu/v1/login",
  function (req, res){
    console.log("POST /apitechu/v1/login");
    var email = req.body.email;
    var password =  req.body.password;
    var id = null;
    console.log("email ["+email+"]");
    console.log("password ["+password+"]");
    var users = require('./data/usuarios.json');
    var encontrado = false;
    var pwderr = false;
    var login =  false;
    var mensaje = null;
    if ( email == null || password == null
         || typeof email == "undefined" || typeof password == "undefined" ){
          console.log("login incorrecto - faltan parametros de entrada");
          mensaje = {"mensaje" : "Login incorrecto - Parametros de entrada no incluidos" };
    } else {
        for (user of users){
          console.log("array-email ["+user.email+"]");
          if ( user.email == email ) {
            console.log("Usuario encontrado correctamente");
            encontrado = true;
            pwderr = (user.password != password);
            if ( user.password == password ){
              login = true;
              user.logged = true;
              id = user.id;
              break;
            }
          }
        }
        if ( login ){
          wireteUserDataToFile(users);
          console.log("Usuario logado persistido");
          mensaje = {"mensaje" : "login correcto", "idUsuario" : id };
        }
        if ( !encontrado ) {
          console.log("login incorrecto - usuario no existe");
          mensaje = {"mensaje" : "Login incorrecto - Usuario no existe" };
        }
        if ( encontrado && pwderr ) {
          console.log("login incorrecto - pwd incorrecta");
          mensaje = {"mensaje" : "Login incorrecto - Password incorrecta" };
        }
    }
    res.send(mensaje);
  }
);


/********* USERS LOGOUT */
app.post("/apitechu/v1/logout",
  function (req, res){
    console.log("POST /apitechu/v1/logout");
    var id = req.body.id;
    var logout = false;
    var notlogin = false;
    var encontrado = false;
    var users = require('./data/usuarios.json');
    if ( id == null || typeof id == "undefined" ){
          console.log("logout incorrecto - faltan parametros de entrada");
          mensaje = {"mensaje" : "Logout incorrecto - Parametros de entrada no incluidos" };
    } else {
        for (user of users){
          console.log("array-id ["+user.id+"]");
          if ( user.id == id ) {
            encontrado = true;
            console.log("Usuario encontrado correctamente");
            if ( user.logged ){
              logout = true;
              delete user.logged;
            } else {
              notlogin = true;
            }
            break
            }
          }
        if ( logout ){
          wireteUserDataToFile(users);
          console.log("Usuario des-logado persistido");
          mensaje = {"mensaje" : "logout correcto", "id" : id };
        } else if ( notlogin ) {
          console.log("logout incorrecto - no logado");
          mensaje = {"mensaje" : "logout incorrecto - usuario no logado"};
        } else if ( ! encontrado ) {
          console.log("logout incorrecto - usuario no existe");
          mensaje = {"mensaje" : "logout incorrecto - usuario no existe"};
        } else  {
          console.log("logout incorrecto");
          mensaje = {"mensaje" : "logout incorrecto"};
        }
    }
    res.send(mensaje);
  }
);



/**
 * Metod de ejemplo para recoger los datos de entrada por todas los posibles opciones
 */
app.post("/apitechu/v1/monstruo/:p1/:p2",
  function (req, res){
    console.log("**********************************");
    console.log("POST /apitechu/v1/monstruo/:p1/:p2");
    console.log("**********************************");


    console.log("\nparams");
    console.log("---------------");
    console.log(req.params.p1);
    console.log(req.params.p2);

    console.log("\nquerystring");
    console.log("---------------");
    console.log(req.query);

    console.log("\nbody");
    console.log("---------------");
    console.log(req.body);

    console.log("\nheaders");
    console.log("---------------");
    console.log(req.headers);
  }

);



/*****************************************************************************/
/*****************************************************************************/
/**************************VERSION 2 *****************************************/
/*****************************************************************************/

/********* USERS LIST v2*/
app.get("/apitechu/v2/users",
  function (req, res){
    console.log("GET /apitechu/v2/users");
    //Creamos un cliente rest, al endpoing  base de nuestra api mlab.
    var httpClient = requestJson.createClient(mLabBaseURL);
    console.log("Cliente http creado.");
    //Peticion de obtencion de usuarios
    httpClient.get("user?"+mLabAPIKey,
      function (err, resMLab, body) {
          var response = !err? body : { "msg" : "Error obteniendo usuarios" };
          //console.log(resMLab);
          res.send(response);
      }
    );
  }
);

/********* GET USERS v2*/
app.get("/apitechu/v2/users/:id",
  function (req, res){
    console.log("GET /apitechu/v2/users/:id");
    //Creamos un cliente rest, al endpoing  base de nuestra api mlab.
    var httpClient = requestJson.createClient(mLabBaseURL);
    console.log("Cliente http creado.");
    var id = req.params.id;
    //Peticion de obtencion de un usuario por id
    var query = 'q={"id":' + id + '}';
    var url = "user?"+query+"&"+mLabAPIKey;
    console.log( url );
    httpClient.get(url,
      function (err, resMLab, body) {
          //var response = !err? body : { "msg" : "Error obteniendo el usuario con id :["  + id + "]" };
          //console.log(resMLab);
          var response = {};
          console.log(err);
          if ( err ) {
            response = { "msg" : "Error obteniendo el usuario con id :["  + id + "]" };
            res.status(500);
          } else if ( body.length > 0 ){
            response = body;
          } else {
            response = { "msg" : "Usuario con id ["  + id + "] no encontrado."};
            res.status(404);
          }

          res.send(response);
      }
    );
  }
);

/********* USERS LOGIN V2*/
app.post("/apitechu/v2/login",
  function (req, res){

    console.log("POST /apitechu/v2/login");
    var email = req.body.email;
    var password =  req.body.password;
    var id = null;
    console.log("email ["+email+"]");
    console.log("password ["+password+"]");

    //Creamos un cliente rest, al endpoing  base de nuestra api mlab.
    var httpClient = requestJson.createClient(mLabBaseURL);
    console.log("Cliente http creado.");
    // cscotson9@sakura.ne.jp && G1rSRIK
    //Peticion de obtencion de un usuario por id
    var query = 'q={"email":"' + email + '"}';
    var url = "user?"+query+"&"+mLabAPIKey;
    console.log( url );
    httpClient.get(url,
      function (err, resMLab, body) {
        console.log(body);
        if ( err ){
          response = { "msg" : "Error realizando login del usuario :["  + email + "]" };
          res.status(500);
        } else if ( body.length == 1  ) {
          if ( body[0].password == password) {
            console.log("login correcto");
            response = {"mensaje" : "login correcto", "idUsuario" : body[0].id };
            var putBody = '{"$set":{"logged":true}}';
            var queryPut = 'q={"email":"' + email + '", "password":"'+password+'"}';
            var urlPut = "user?"+query+"&"+mLabAPIKey;
            httpClient.put(urlPut, JSON.parse(putBody),
              function(errPut, resPut, bodyPut){
                  if ( err ) {
                    console.log("login incorrecto - put");
                    response = { "msg" : "Error realizando login del usuario :["  + email + "]" };
                    res.status(500);
                  } else {
                    console.log("login correcto - put");
                  }
                  res.send(response);
              }
            );
          } else  {
            console.log("login incorrecto - pwd incorrecta");
            response = {"mensaje" : "Login incorrecto - Password incorrecta" };
            res.status(404);
            res.send(response);
          }
        } else if ( body.length > 1  ) {
          console.log("login incorrecto - mas de un usuario con ese email");
          response = {"mensaje" : "Login incorrecto - mas de un usuario con ese email" };
          res.status(404);
          res.send(response);
        } else {
          console.log("login incorrecto - usuario no existe");
          response = {"mensaje" : "Login incorrecto - Usuario no existe" };
          res.status(404);
          res.send(response);
        }

      }
    );
  }
);



/********* USERS LOGOUT V2*/
app.put("/apitechu/v2/logout",
  function (req, res){

    console.log("PUT /apitechu/v2/logout");
    var id = req.body.id;
    console.log("id ["+id+"]");

    //Creamos un cliente rest, al endpoing  base de nuestra api mlab.
    var httpClient = requestJson.createClient(mLabBaseURL);
    console.log("Cliente http creado.");
    // cscotson9@sakura.ne.jp && G1rSRIK
    //Peticion de obtencion de un usuario por id
    var query = 'q={"id":' + id + '}';
    var url = "user?"+query+"&"+mLabAPIKey;
    console.log( url );
    httpClient.get(url,
      function (err, resMLab, body) {
        console.log(body);
        if ( err ){
          response = { "msg" : "Error realizando logout del usuario con id:["  + id + "]" };
          res.status(500);
        } else if ( body.length == 1  ) {
          if ( body[0].logged ) {
            console.log("logout correcto");
            response = {"mensaje" : "logout correcto", "idUsuario" : body[0].id };
            var putBody = '{"$unset":{"logged":true}}';
            var queryPut = 'q={"id":' + id + '}';
            var urlPut = "user?"+query+"&"+mLabAPIKey;
            httpClient.put(urlPut, JSON.parse(putBody),
              function(errPut, resPut, bodyPut){
                  if ( err ) {
                    console.log("logout incorrecto - put");
                    response = { "msg" : "Error realizando logout del usuario con id :["  + id + "]" };
                    res.status(500);
                  } else {
                    console.log("logout correcto - put");
                  }
                  res.send(response);
              }
            );
          } else {
            console.log("logout incorrecto - no logado");
            response = {"mensaje" : "logout incorrecto - usuario no logado"};
            res.status(404);
            res.send(response);
          }
        } else {
          console.log("logout incorrecto - usuario no existe");
          response = {"mensaje" : "logout incorrecto - usuario no existe"};
          res.status(404);
          res.send(response);
        }
      }
    );
  }
);

/** GET ACCOUNTS V2*/
app.get("/apitechu/v2/:id/accounts",
  function (req, res){
    console.log("GET /apitechu/v2/:id/accounts");
    var id = req.params.id;
    console.log("id ["+id+"]");

    //Creamos un cliente rest, al endpoing  base de nuestra api mlab.
    var httpClient = requestJson.createClient(mLabBaseURL);
    console.log("Cliente http creado.");
    // cscotson9@sakura.ne.jp && G1rSRIK
    //Peticion de obtencion de un usuario por id
    var query = 'q={"userid":' + id + '}';
    var url = "account?"+query+"&"+mLabAPIKey;
    console.log( url );
    httpClient.get(url,
      function (err, resMLab, body) {
        console.log(body);
        if ( err ){
          response = { "msg" : "Error obteniendo las cuentas del usuario con id:["  + id + "]" };
          res.status(500);
        } else if ( body.length == 0 ){
          response = { "msg" : "El usuario con id:["  + id + "], no tiene cuentas asociadas." };
          res.status(404);
        } else {
            console.log("get accounts correcto");
            console.log(body);
            response = body[0];
        }

        res.send(response);
      }
    );
  }
);




/*****************************************************************************/
/*****************************************************************************/
/**************************UTILIDADES ****************************************/
/*****************************************************************************/
/** Almacena en disco el contenido de data .
 *  El fichero destino es el ./data/usuarios.json
 */
function wireteUserDataToFile ( data ){
  var jsonUserData = JSON.stringify( data );
  fs.writeFile("./data/usuarios.json",
               jsonUserData,
               "UTF-8",
               function(err){
                  if (err){
                    console.log(err);
                  } else {
                    console.log("Datos escritos con exito");
                  }
               });
}
